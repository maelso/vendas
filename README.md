# Vendas



## Começando

- [ ] [Adicionar arquivos usando a linha de comando](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) ou enviar um repositório Git existente com o seguinte comando:

```
cd existing_repo
git remote add origin https://gitlab.com/maelso/vendas.git
git branch -M <branch_name>
git push -uf origin <branch_name>
```


## Especificação do Projeto - Sistema de Gestão de Usuários, Produtos e Vendas com FastAPI

### Equipe
- José João
- Eriaerton
- Pedro Duardo 
- Felipe Monteiro 
- Lucas Lima da Costa

### Visão Geral
Este projeto visa desenvolver um sistema de gestão completo, incluindo CRUD de usuários, produtos e vendas. Será utilizado o framework FastAPI para criar a API, SQLite como banco de dados e Postman para testar a API.

### Requisitos Funcionais
1. **Usuários**:
   - Criação de usuários com as informações: nome, data de nascimento, CPF, contato e endereço.
   - Listagem de todos os usuários cadastrados.
   - Recuperação de um usuário específico por ID.
   - Atualização das informações de um usuário.
   - Exclusão de um usuário.

2. **Produtos**:
   - Criação de produtos com as informações: nome, descrição, preço e estoque.
   - Listagem de todos os produtos cadastrados.
   - Recuperação de um produto específico por ID.
   - Atualização das informações de um produto.
   - Exclusão de um produto.

3. **Vendas**:
   - Registro de vendas associadas a um usuário e a um ou mais produtos.
   - Listagem de todas as vendas realizadas.
   - Recuperação de uma venda específica por ID.
   - Atualização das informações de uma venda.
   - Exclusão de uma venda.

## Arquitetura
- **Framework Web**: FastAPI (https://fastapi.tiangolo.com/)
- **Banco de Dados**: SQLite

## Estrutura de Dados
### Tabelas do Banco de Dados
1. **Usuario**
    - id (autoincremento)
    - nome
    - data de nascimento
    - cpf (validação do CPF)
    - id_contato (chave estrangeira)
    - id_endereco (chave estrangeira)

2. **Contato**
    - id (autoincremento)
    - email
    - telefone
    - id_usuario (chave estrangeira)

3. **Endereco**
    - id (autoincremento)
    - rua
    - numero
    - complemento (opcional)
    - bairro
    - cidade
    - pais (default=Brasil)

4. **Produto**
    - id (autoincremento)
    - nome
    - descricao
    - preco
    - estoque

5. **Venda**
    - id (autoincremento)
    - id_usuario (chave estrangeira)
    - data_venda

6. **ItemVenda**
    - id (autoincremento)
    - id_venda (chave estrangeira)
    - id_produto (chave estrangeira)
    - quantidade

## Endpoints da API
- **Usuários**:
    - **POST /usuarios/**: Cria um novo usuário.
    - **GET /usuarios/**: Retorna a lista de todos os usuários cadastrados.
    - **GET /usuarios/{id}/**: Retorna as informações de um usuário específico.
    - **PUT /usuarios/{id}/**: Atualiza as informações de um usuário.
    - **DELETE /usuarios/{id}/**: Exclui um usuário.

- **Produtos**:
    - **POST /produtos/**: Cria um novo produto.
    - **GET /produtos/**: Retorna a lista de todos os produtos cadastrados.
    - **GET /produtos/{id}/**: Retorna as informações de um produto específico.
    - **PUT /produtos/{id}/**: Atualiza as informações de um produto.
    - **DELETE /produtos/{id}/**: Exclui um produto.

- **Vendas**:
    - **POST /vendas/**: Registra uma nova venda.
    - **GET /vendas/**: Retorna a lista de todas as vendas realizadas.
    - **GET /vendas/{id}/**: Retorna as informações de uma venda específica.
    - **PUT /vendas/{id}/**: Atualiza as informações de uma venda.
    - **DELETE /vendas/{id}/**: Exclui uma venda.

## Ferramentas Utilizadas
- **FastAPI**: Framework web para criação da API.
- **SQLite**: Banco de dados local.
- **Postman**: Ferramenta para testar a API.
- **GitLab**: Plataforma de versionamento de código.

## Atividades por Integrante
    - A contribuição de cada integrante será avaliada a partir das contribuições registradas GitLab

## Controle de Versão
- O código fonte será versionado no GitLab.
- Cada tarefa terá uma branch específica e essa branch deve ser criada a partir da branch main.


## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/maelso/vendas/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

